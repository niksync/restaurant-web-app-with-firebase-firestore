import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

// Replace this with your own config details
var config = {
    apiKey: "AIzaSyBcNZZPnkKnMUOa3HHs67Bm1OQc8vrnf7o",
    authDomain: "del-rio-2.firebaseapp.com",
    databaseURL: "https://del-rio-2.firebaseio.com",
    projectId: "del-rio-2",
    storageBucket: "del-rio-2.appspot.com",
    messagingSenderId: "1016516935859",
    appId: "1:1016516935859:web:7d6fd0009046ccdc"
  };


// Initialize Firebase
firebase.initializeApp(config);
// firebase.firestore().settings({ timestampsInSnapshots: true });
export default firebase 