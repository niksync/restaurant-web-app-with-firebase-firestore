import * as ActionTypes from '../reducers/ActionTypes';

export const fetchDishes = () => (dispatch) => {
    dispatch(dishesLoading(true));

 return (dispatch, getState, {getFirebase, getFirestore} ) => {
     const firestore = getFirestore();
     console.log("searching firestore");

 firestore.collection('dishes').get()
        .then(snapshot => {            
            let dishes = [];
            snapshot.forEach(doc => {
                const data = doc.data()
                const _id = doc.id
                dishes.push({_id, ...data });
            });
            return dishes;
        })
        .then(dishes => dispatch(addDishes(dishes)))
        .catch(error => dispatch(dishesFailed(error.message)));
}}

export const dishesLoading = () => ({
    type: ActionTypes.DISHES_LOADING
});
export const dishesFailed = (errmess) => ({
    type: ActionTypes.DISHES_FAILED,
    payload: errmess
});
export const addDishes = (dishes) => ({
    type: ActionTypes.ADD_DISHES,
    payload: dishes
});