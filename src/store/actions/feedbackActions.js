export const postFeedback = (feedback) => {
        
    return (dispatch, getState, {getFirebase, getFirestore} ) => {

    const firestore = getFirestore();
    firestore.collection('feedback').add({
        ...feedback,
        createdAt: new Date()
    }).then(response => { console.log('Feedback', response); 
            alert('Thank you for your feedback!'); })
    .catch((error) =>  { console.log('Feedback', error.message);
     alert('Your feedback could not be posted\nError: '+error.message); });
    }
}
