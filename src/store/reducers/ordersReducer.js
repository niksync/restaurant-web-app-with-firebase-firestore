const initState = {
    orders: []
  }
  
  const ordersReducer = (state = initState, action) => {
    switch (action.type) {
      case 'CREATE_ORDER':
        console.log('create order', action.order);
        return state;
      case 'CREATE_ORDER_ERROR':
        console.log('create order error', action.err);
        return state;
      default: 
        return state;
    }
  }
  
  export default ordersReducer;