import authReducer from './authReducer'
import dishesReducer from './dishesReducer'
import ordersReducer from './ordersReducer'
import commentsReducer from './commentsReducer'
import { InitialFeedback } from './forms';
import { createForms } from 'react-redux-form';

import { combineReducers } from 'redux'
import {firestoreReducer} from 'redux-firestore'
import { firebaseReducer } from 'react-redux-firebase'


const rootReducer = combineReducers({
  auth: authReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer,
  dishes: dishesReducer,
  orders: ordersReducer,
  comments: commentsReducer,
  //promotions: promotionsReducer,
  //leaders: leadersReducer,
  //orders: ordersReducer,
  ...createForms({
    feedback: InitialFeedback
              }),
 
});

export default rootReducer;

// the key name will be the data property on the state object