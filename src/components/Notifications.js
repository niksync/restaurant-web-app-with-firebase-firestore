import React from 'react';
import { Card } from 'reactstrap';
import moment from 'moment';


    function RenderMenuItem({ notification}) {
        return(
            <Card className="m-2">
                        <p>{notification.authorFirstName} {notification.authorLastName}</p>
                           <p> order for <strong>{notification.ordernumber}</strong> servings, </p>
                        <p> <strong>{notification.dishname}</strong>  serving size: <strong>{notification.size}</strong> </p>
                        <p>Contact no. {notification.phonenumber}</p>
                        <p>Contact type {notification.contacttype}</p>
                        <p>Deliver to {notification.location}</p>
                        <p> {moment(notification.createdAt.toDate()).calendar()}</p>
                        <p><button>Update</button></p>
                        <p><button>Delete</button></p>
            </Card>
        );
    }

    const Notifications = ( {notifications}) => {
        
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <h3>Notifications</h3>
                            <hr />
                        </div>
                    </div>
                    <div className="row">                      
                           {notifications && notifications.map((notification) => {
                                    return (
                                        <div key={notification.id} className="col-12 col-md-3 m-1">
                                            <RenderMenuItem notification={notification} />
                                        </div>
                                    );
                                })
                            }
                       
                    </div>
                </div>
            );
    }

  export default Notifications              
