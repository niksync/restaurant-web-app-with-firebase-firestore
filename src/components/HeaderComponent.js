import React, { Component } from 'react';
import {Nav, Navbar, NavbarBrand, NavbarToggler, Collapse,
     NavItem, Jumbotron, Button, Modal, ModalHeader, ModalBody, 
     Form, FormGroup, Input, Label } from 'reactstrap';
import {NavLink } from 'react-router-dom';
import {signUp} from '../store/actions/authActions'
import {signIn} from '../store/actions/authActions'
import { signOut } from '../store/actions/authActions'
import {connect} from 'react-redux'


class Header extends Component {

    constructor(props){
        super(props);
        this.toggleNav = this.toggleNav.bind(this);        
        this.toggleModal = this.toggleModal.bind(this);
        this.toggleModal2 = this.toggleModal2.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
        this.handleSignUp = this.handleSignUp.bind(this);
        this.handleLogout = this.handleLogout.bind(this);
  
        this.state = {
            isNavOpen:false,
            isModalOpen: false,
            isModal2Open: false

        };
    }

    toggleNav() {
        this.setState({
            isNavOpen: !this.state.isNavOpen
        });
    }
    toggleModal() {
        this.setState({
          isModalOpen: !this.state.isModalOpen
        });
      }
    toggleModal2() {
        this.setState({
          isModal2Open: !this.state.isModal2Open
        });
      }

      handleLogin(event) {
        this.toggleModal();
        this.props.signIn({email: this.email.value, password: this.password.value});
        event.preventDefault();
      }
      handleSignUp(event) {
        this.toggleModal2();
        this.props.signUp({email: this.email.value,
                           password: this.password.value,
                           firstName: this.firstName.value,
                           lastName:this.lastName.value});
        event.preventDefault();
      }
    
    handleLogout() {
        this.props.signOut();
    }

    render(){
        const { auth, authError,profile } = this.props;
        return(

            <React.Fragment>                
                 <Navbar dark  color="primary" expand="md">
                        <div className = "container">
                            <NavbarToggler onClick={this.toggleNav} />

                            <NavbarBrand className="mr-auto" href="/">
                                < img src="./assets/images/logo.png" height="30" width="41"  alt= "Del Rio Food Court"/>
                            </NavbarBrand>
                            <Collapse isOpen={this.state.isNavOpen} navbar>
                                <Nav navbar>
                                    <NavItem>
                                        <NavLink className="nav-link"  to='/home'>
                                        <span className="fa fa-home fa-lg"></span> Home</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink className="nav-link" to='/aboutus'><span className="fa fa-info fa-lg"></span> About Us</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink className="nav-link"  to='/menu'><span className="fa fa-list fa-lg"></span> Menu</NavLink>
                                    </NavItem>
                                    <NavItem>
                             {auth.uid? <NavLink className="nav-link" to="/favorites"><span className="fa fa-heart fa-lg"></span> My Favorites</NavLink> : null}
                                    </NavItem>
                                    <NavItem>
                        {profile.isAdmin?
                        <NavLink className="nav-link" to="/notifications"><span className="fa fa-user fa-lg"></span> Notifications</NavLink>
                            : null}  
                                    </NavItem>
                                    <NavItem>
                                        <NavLink className="nav-link" to='/contactus'><span className="fa fa-address-card fa-lg"></span> Contact Us</NavLink>
                                    </NavItem>
                                    <div color="danger">  {authError ? <p>{ authError }</p> :null}</div>
                                </Nav>
                                <Nav className="ml-auto" navbar>
                                    <NavItem>
                                        { !this.props.auth.uid ?
                                            <div>
                                            <Button color="yellow" outline onClick={this.toggleModal}>
                                                <span className="fa fa-sign-in fa-lg"></span> Login                                                
                                            </Button>
                                            <Button color="yellow" outline onClick={this.toggleModal2}>
                                                <span className="fa fa-sign-in fa-lg"></span> SignUp
                                               
                                            </Button>
                                            </div>
                                            :
                                            <div>
                                                <div className="navbar-text mr-3">
                                                {/* {this.props.auth.user.displayName} */}
                                                </div>
                                                    <Button color="yellow" outline onClick={this.handleLogout}>
                                                        <span  className="fa fa-sign-out fa-lg"></span> Logout
                                                        {/* {this.props.auth.isFetching ?
                                                            <span className="fa fa-spinner fa-pulse fa-fw"></span>
                                                            : null
                                                        } */}
                                                    </Button>
                                            </div>
                                        }

                                    </NavItem>
                            </Nav>
                            </Collapse>
                           

                        </div>
                    </Navbar>
                    <Jumbotron>
                        <div className="container">
                            <div className= "row row-header">
                                <div className="col-12 col-sm-6">
                                    <h1>Del Rio Food Court</h1>
                                    <p>We take inspiration from the World</p>                            
                                </div>
                            </div>
                        </div>
                    </Jumbotron>

                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Login</ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.handleLogin}>
                            <FormGroup>
                                <Label htmlFor="email">Email</Label>
                                <Input type="text" id="email" name="email"
                                    innerRef={(input) => this.email = input} />
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="password">Password</Label>
                                <Input type="password" id="password" name="password"
                                    innerRef={(input) => this.password = input}  />
                            </FormGroup>
                            <FormGroup check>
                                <Label check>
                                    <Input type="checkbox" name="remember"
                                    innerRef={(input) => this.remember = input}  />
                                    Remember me
                                </Label>
                            </FormGroup>
                            <Button type="submit" value="submit" color="primary">Login</Button>
                        </Form>
                        {/* <p></p>
                        <Button color="danger" onClick={this.handleGoogleLogin}><span className="fa fa-google fa-lg"></span> Login with Google</Button> */}
                   
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.isModal2Open} toggle={this.toggleModal2}>
                    <ModalHeader toggle={this.toggleModal2}>SignUp</ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.handleSignUp}>
                            <FormGroup>
                                <Label htmlFor="email">Email</Label>
                                <Input type="text" id="email" name="email"
                                    innerRef={(input) => this.email = input} />
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="password">Password</Label>
                                <Input type="password" id="password" name="password"
                                    innerRef={(input) => this.password = input}  />
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="firstName">First Name</Label>
                                <Input type="text" id="firstName" name="firstName"
                                    innerRef={(input) => this.firstName = input} />
                            </FormGroup>
                            <FormGroup>
                                <Label htmlFor="lastName">Last Name</Label>
                                <Input type="text" id="lastName" name="lastName"
                                    innerRef={(input) => this.lastName = input} />
                            </FormGroup>
                           
                            <Button type="submit" value="submit" color="primary">Sign Up</Button>
                        </Form>
                        <p></p>
                        {/* <Button color="danger" onClick={this.handleGoogleLogin}><span className="fa fa-google fa-lg"></span> Login with Google</Button> */}
                    </ModalBody>
                </Modal>
                
            </React.Fragment>
        )}
    
    }
    const mapStateToProps = (state) => { 
        // console.log(state)

        return { 
          authError:state.auth.authError,
          auth:state.firebase.auth,
          profile:state.firebase.profile
        }
      }
      const mapDispatchToProps = (dispatch) => {
        return { 
          signUp:(creds) =>dispatch(signUp(creds)),
          signIn: (creds) => dispatch(signIn(creds)),
          signOut:() => dispatch(signOut())
        }
      }
export default connect(mapStateToProps,mapDispatchToProps) (Header);