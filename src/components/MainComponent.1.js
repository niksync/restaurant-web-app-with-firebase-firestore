import React, { Component } from 'react';
import Home from './HomeComponent';
import Menu from './MenuComponent';
import DishDetail from './DishdetailComponent';
import Contact from './ContactComponent';
import About from './AboutComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Notifications from './Notifications';
import { Switch, Route, Redirect,withRouter } from 'react-router-dom'
import { connect } from 'react-redux';
import {createComment} from '../store/actions/commentActions'
import {createOrder} from '../store/actions/orderActions'
import { postFeedback} from '../store/actions/feedbackActions'
import {firestoreConnect } from 'react-redux-firebase'
import {compose} from 'redux'
import { actions } from 'react-redux-form';

    
  const mapDispatchToProps = (dispatch) => ({  
    createOrder:(order) =>dispatch(createOrder(order)),
    createComment:(comment) =>dispatch(createComment(comment)),
    postFeedback:(feedback) =>dispatch(postFeedback(feedback)),
    resetFeedbackForm: () => { dispatch(actions.reset('feedback'))},

  } )

class Main extends Component {    
  render() { 
    const { dishes,leaders,promotions,comments, auth,createOrder, createComment, orders, postFeedback} = this.props;
      // console.log(dishes)       
   

    const DishWithId = ({match}) => {
    const dishid = match.params.id;
    const dishcomments= comments && comments.filter(comment => comment.dishId === dishid );
           
    const dish= dishes && dishes.find(dish1 => dish1.id === dishid );
    // console.log(dish)       

    return(
     <DishDetail dish={dish} auth={auth} comments={dishcomments} createComment= {createComment} createOrder = {createOrder} />
          )
      }
    return (
      <div className='bg-info'>
        <Header />        
          <Switch>
            <Route path="/home" component={()=> <Home dishes={dishes} leaders={leaders} promotions={promotions}/> } />
            <Route exact path="/menu" component={() => <Menu dishes={dishes} auth={auth}/> } />
            <Route path="/menu/:id" component={DishWithId} />
            <Route path="/notifications" component= {() => <Notifications notifications={orders}/> } />
            <Route path="/aboutus" component={() => <About leaders={leaders} auth={auth}/> } />
            <Route exact path='/contactus'  component={() => <Contact resetFeedbackForm={this.props.resetFeedbackForm} auth={auth} postFeedback= {postFeedback}/>} />
            <Redirect to="/home" />
          </Switch>        
        <Footer />
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  console.log (state.firestore.ordered.orders)

  return {
    dishes: state.firestore.ordered.dishes,
    leaders: state.firestore.ordered.leaders,
    promotions: state.firestore.ordered.promotions,
    comments: state.firestore.ordered.comments,
    orders: state.firestore.ordered.orders,
    notifications: state.firestore.ordered.notifications,
    auth:state.firebase.auth
  }
}
export default withRouter(compose(
  connect(mapStateToProps,mapDispatchToProps),
  firestoreConnect([
    {collection: 'dishes', orderBy: ['createdAt']},
    {collection:'leaders'},
    {collection:'promotions'},
    {collection:'comments', orderBy: ['createdAt', 'desc']},
    {collection:'orders',orderBy: ['createdAt', 'desc']},
    {collection:'notifications',orderBy: ['createdAt', 'desc']},
  ])
  )(Main))
  