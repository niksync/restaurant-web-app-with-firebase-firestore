import React from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle } from 'reactstrap';
import DemoCarousel from './Carousel3';
// import { FadeTransform } from 'react-animation-components';
import { Link } from 'react-router-dom';

function RenderCard({item}) {    
        return(
                <Card>
                    <CardImg width="80%" height="280vh" src={item.image} alt={item.name} />
                    <CardBody>
                        <CardTitle>{item.name}</CardTitle>
                        {item.designation ? <CardSubtitle>{item.designation}</CardSubtitle> : null}
                        <CardText>{item.description}</CardText>
                    </CardBody>
                </Card>
        );
}

function Home(props) {
  const {dishes,leaders,promotions}=  props;
    return(
        <div className="container">
             <div className="col-12 col-md m-1">
             <DemoCarousel />
             </div>
            
            <div className="row align-items-center">           
                <div className="col-12 col-md m-1">
                           {dishes && dishes.map((dish) => {
                               if (dish.featured)
                                    return (
                                        <Link to='/menu' >
                                            <div key={dish.id} >
                                                <RenderCard item={dish} />
                                            </div>
                                        </Link>
                                            );
                                        })
                            }                       
                </div>
                <div className="col-12 col-md m-1">
                           {promotions && promotions.map((promo) => {
                               if (promo.featured)
                                    return (
                                        <Link to='/menu' >
                                            <div key={promo.id} >
                                                <RenderCard item={promo} />
                                            </div>
                                        </Link>

                                            );
                                        })
                            }                       
                </div>
                {/* <div className="col-12 col-md m-1">
                           {leaders && leaders.map((leader) => {
                               if (leader.featured)
                                    return (
                                        <Link to='/aboutus' >
                                            <div key={leaders.id} >
                                                <RenderCard item={leader} />
                                            </div>         
                                        </Link>
                                            );
                                        })
                            }                       
                </div> */}
            </div>
                    

        </div>
    );
}

  export default Home;
  