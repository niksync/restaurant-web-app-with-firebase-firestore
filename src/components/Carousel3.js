import React, { Component } from 'react';
import '../../node_modules/react-responsive-carousel/lib/styles/carousel.min.css'
import { Carousel } from 'react-responsive-carousel';
import { Link } from 'react-router-dom';

class DemoCarousel extends Component {
    render() {
        return (
            <Carousel>
                <div>
                    <img alt='dish1'src='https://firebasestorage.googleapis.com/v0/b/del-rio-2.appspot.com/o/images%2Fbankutilapia.jpg?alt=media&token=a08c02ae-aa36-40a2-92ff-84140b5db12a'/>
                    <Link to='/menu' > <p className="legend">Legend 1</p></Link>
                </div>
                <div>
                    <img alt='dish2' src='https://firebasestorage.googleapis.com/v0/b/del-rio-2.appspot.com/o/images%2Ffriedrice.jpg?alt=media&token=392e7bfc-d8ee-40a0-88c8-f5bda4e8c569' />
                    <p className="legend">Legend 2</p>
                </div>
                <div>
                    <img alt='dish3' src= 'https://firebasestorage.googleapis.com/v0/b/del-rio-2.appspot.com/o/images%2Fjollofrice.jpg?alt=media&token=a5986cb6-c124-413c-88df-c4d6558cca7b' />
                    <p className="legend">Legend 3</p>
                </div>
            </Carousel>
        );
    }
}
 export default DemoCarousel