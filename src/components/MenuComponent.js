import React from 'react';
import { Card, CardImg, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { Link } from 'react-router-dom';

    function RenderMenuItem({ dish, onClick }) {
        return(
            <Card>
                <Link to={`/menu/${dish.id}`} >
                    <CardImg width="100%" src={dish.image} alt={dish.name} />
                        <CardTitle>{dish.name}</CardTitle>
                </Link>
            </Card>
        );
    }

    const Menu = ( {dishes}) => {
        
            return (
                <div className="container bg-dark"  >
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
                            <BreadcrumbItem active>Menu</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12 row"class="text-light">
                            <h3>Menu</h3>
                            <hr />
                        </div>
                    </div>
                    <div className="row">                      
                           {dishes && dishes.map((dish) => {
                                    return (
                                        <div key={dish.id} className="col-12 col-md-5 m-1">
                                            <RenderMenuItem dish={dish} />
                                        </div>
                                    );
                                })
                            }
                       
                    </div>
                </div>
            );
    }

export default Menu;