import React from 'react';
import { Breadcrumb, BreadcrumbItem, Card, CardBody, CardHeader, Media } from 'reactstrap';
import { Link } from 'react-router-dom';
import {Redirect } from 'react-router-dom'

function About( {leaders, auth}) {
    function  RenderLeader({leader}) {
        return (
            <div key={leader.id} className= "row row-content">
                {/* <div className="col-12 col-md-2"> */}
                <Media tag="li">
                        <Media left middle>
                            <Media object src={leader.image} alt={leader.name}/>
               
                            <Media body className="ml-5">
                                <h4> {leader.name}</h4>
                                <p>{leader.designation}</p>
                                <p>{leader.description}</p>                
                            </Media>
                        </Media>
                </Media>
            </div>
            
        );
    }

if(!auth.uid) return <Redirect to ='/home'/>

    return(
        <div className="container" >
            <div className="row">
                <Breadcrumb>
                    <BreadcrumbItem><Link to="/home">Home</Link></BreadcrumbItem>
                    <BreadcrumbItem active>About Us</BreadcrumbItem>
                </Breadcrumb>
                <div className="col-12">
                    <h3>About Us</h3>
                    <hr />
                </div>                
            </div>
            <div className="row row-content">
                <div className="col-12 col-md-6  text-white">
                    <h2>Our History</h2>
                    <p>Started in May 2019, Del Rio is a family business  established as a culinary icon.</p>
                    <p>It aims to serve the best in local dishes, in a family-friendly environment where the everyone can eat and relax</p>
                </div>
                <div className="col-12 col-md-5">
                    <Card>
                        <CardHeader className="bg-primary text-white">Facts At a Glance</CardHeader>
                        <CardBody>
                            <dl className="row p-1">
                                <dt className="col-6">Started</dt>
                                <dd className="col-6">9 May. 2019</dd>
                                <dt className="col-6">Major Stake Holder</dt>
                                <dd className="col-6">Phoenix Innovations Ltd</dd>                               
                                <dt className="col-6">Employees</dt>
                                <dd className="col-6">10</dd>
                            </dl>
                        </CardBody>
                    </Card>
                </div>
                <div className="col-12">
                    <Card>
                        <CardBody className="bg-faded">
                            <blockquote className="blockquote">
                                <p className="mb-0">You better cut the pizza in four pieces because
                                    I'm not hungry enough to eat six.</p>
                                <footer className="blockquote-footer">Yogi Berra,
                                <cite title="Source Title">The Wit and Wisdom of Yogi Berra,
                                    P. Pepe, Diversion Books, 2014</cite>
                                </footer>
                            </blockquote>
                        </CardBody>
                    </Card>
                </div>
            </div>
            <div className="row row-content">
                <div className="col-12">
                    <h2>Corporate Leadership</h2>
                </div>
                <div >
                    <Media list>
                    {leaders && leaders.map((leader) => {
                        return (  <RenderLeader leader = {leader} />);
    })
}
                    </Media>
                </div>
            </div>
        </div>
    );
}

export default About;    