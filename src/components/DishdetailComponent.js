import React, { Component } from 'react';
import { Card, CardImg,Form, FormGroup, Input, CardText, CardBody,CardTitle, Breadcrumb, BreadcrumbItem, Button, Modal, ModalHeader, ModalBody, Label} from 'reactstrap';
import {Link} from 'react-router-dom';
// import {Control, LocalForm, Errors} from 'react-redux-form';
import {Redirect } from 'react-router-dom'
import moment from 'moment';
// const maxLength = (len) => (val) => !(val) || (val.length <= len);
// const minLength = (len) => (val) => val && (val.length >= len);
// const required = (val) => val && val.length;
// const isNumber = (val) => !isNaN(Number(val));

    function RenderDish ({dish,dishId, createOrder}) { 
        // console.log(createOrder)           
        return (
            <div className="row">
                <div className="col-12 align-items-center col-md-7 m-1">
                    <Card>
                        <CardImg top src={dish.image} alt={dish.name} />
                        <CardBody>
                            <CardTitle>{dish.name}</CardTitle>
                            <CardText>{dish.description}</CardText>
                        </CardBody>
                    </Card>
                </div>
                <div className="col-12 col-md-3 m-1">
                    <Card  className="bg-danger text-white">               
                        <CardBody>
                        <OrderForm dishId={dishId}
                                    dish={dish} 
                                    createOrder={createOrder} 
                                    />
                        </CardBody>
                    </Card>
                </div>
            </div>
        );
}
class OrderForm extends Component{

    constructor(props) {
        super(props);
   
        this.toggleModal = this.toggleModal.bind(this);
        this.handleSubmit= this.handleSubmit.bind(this);

        this.state = {
            isModalOpen: false,
            isNavOpen:false
        };
    }

    toggleModal() {
        this.setState({
          isModalOpen: !this.state.isModalOpen
        });
    }

    handleSubmit(event) {
        this.toggleModal();
        this.props.createOrder(  { dishname:this.props.dish.name,
                                    dishId:this.props.dishId, 
                                    size:this.size.value,
                                    ordernumber:this.ordernumber.value, 
                                    details:this.details.value, 
                                    author: this.author.value,
                                    location:this.location.value, 
                                    contacttype:this.contacttype.value,
                                    phonenumber:this.phonenumber.value
                                     });
        event.preventDefault();
        console.log(this.props.dishId)

    }

    render(){
        return(
            <div>
                <Button color="yellow" outline onClick={this.toggleModal}> Order your meal</Button>
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                <ModalBody> 
                    <Form onSubmit={this.handleSubmit}>
                        
                        <FormGroup>
                            <Label htmlFor="size" md={5}>Size of meal</Label>
                            <Input type="select" name="size" id="size" 
                                            innerRef={(input) => this.size = input}>                  
                                            <option >Kiddie </option>
                                            <option >Small</option>
                                            <option >Medium</option>
                                            <option>Large</option>                            
                            </Input>                              
                        </FormGroup>
                        <FormGroup>                            
                            <Label htmlFor="ordernumber" md={5}>Number of meals</Label>
                            <Input type="select"  id="ordernumber" name="ordernumber"
                                            innerRef={(input) => this.ordernumber = input}>                  
                                            <option >1</option>
                                            <option >2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                            </Input>
                        </FormGroup>
                        <FormGroup >
                            <Label htmlFor="details" md={5}>Further details</Label>                                  
                            <Input type="textarea" id="details" name="details"
                                rows="2"
                                innerRef={(input) => this.details = input} />
                        </FormGroup>  
                        <FormGroup>
                            <Label htmlFor="author" md={5}>Your Name</Label>
                            <Input type="text"md={5} id="author" name="author"
                                innerRef={(input) => this.author = input} />
                        </FormGroup>
                        <FormGroup>
                                <Label htmlFor="location" md={5}>Your Location</Label>
                                    <Input type= "text" id="location" name="location"
                                        placeholder="Location within Accra"
                                        innerRef={(input) => this.location = input}/>
                        </FormGroup> 
                        <FormGroup>                           
                            <Label htmlFor="contacttype" md={8}>how may we contact you?</Label>
                            <Input type="select" id ="contacttype" name="contacttype"
                                            innerRef={(input) => this.contacttype = input}>                  
                                            <option  >Mobile</option>
                                            <option >Whatsapp</option>
                            </Input>                          
                        </FormGroup>                       
                        <FormGroup>
                                    <Label htmlFor="phonenumber" md={5}>phonenumber</Label>
                                    <Input type="text" id="phonenumber" name="phonenumber" placeholder="phonenumber"
                                             innerRef={(input) => this.phonenumber = input}/>
                        </FormGroup>      
                               
                                <FormGroup>
                                        <Button type="submit" onClick={this.toggleModal} color="primary">
                                            Submit
                                        </Button>
                                </FormGroup>
                    </Form>
                            
                </ModalBody>
                </Modal>
            </div>
        );
    }

}


    function RenderComments({comments, dish, dishId, createComment}) {
        // console.log({comments, dish})
        // if (comments != null)
            return(
                <div className="col-12 col-md-5 m-1">
                    <h4>Comments</h4>
                    <div>
                        <ul className="list-unstyled">
                            { comments && comments.map((comment) => {
                                   return (
                                    <div  key={comment.id}>
                                        <li>
                                        <p>{comment.comment}</p>
                                        <p>{comment.rating} stars</p>
                                        <p>-- <em>{comment.authorFirstName}</em>  ,{moment(comment.createdAt.toDate()).calendar()}</p>
                                        <p>-------------------------</p>
                                        </li>
                                    </div>
                                );
                                })}
                        </ul>   
                        <CommentForm dishId={dishId} 
                                createComment={createComment} />                 
                    </div>
                   
                </div>
                    )
            
                }

    class CommentForm extends Component{

        constructor(props) {
            super(props);
       
            this.toggleModal = this.toggleModal.bind(this);
            this.handleSubmit= this.handleSubmit.bind(this);

            this.state = {
                isModalOpen: false,
                isNavOpen:false
            };
        }

        toggleModal() {
            this.setState({
              isModalOpen: !this.state.isModalOpen
            });
        }

        handleSubmit(event) {
            this.toggleModal();
            this.props.createComment(  { dishId:this.props.dishId, 
                                        rating: this.rating.value, 
                                        // author: this.author.value,
                                        comment:this.comment.value});
            event.preventDefault();
            console.log(this.props.dishId)

        }

        render(){
            return(
                <div>
                    <Button color="yellow" outline onClick={this.toggleModal}> Submit Comment</Button>
                    <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                    <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
                    <ModalBody> 
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                
                                <Label htmlFor="rating" md={5}>Rating</Label>
                                <Input type="select" name="rating" id="rating"
                                innerRef={(input) => this.rating = input}>                                  
                                                <option >1</option>
                                                <option selected>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                    </Input>  
                                
                            </FormGroup>
                                    <FormGroup>
                                            <Label htmlFor="comment" md={5}>Your Feedback</Label>                                   
                                            <Input type="textarea" id="comment" name="comment"
                                                innerRef={(input) => this.comment = input} />
                                    </FormGroup>
                                    <FormGroup>
                                            <Button type="submit" onClick={this.toggleModal} color="primary">
                                                Submit
                                            </Button>
                                    </FormGroup>
                        </Form>
                                
                    </ModalBody>
                    </Modal>
                </div>
            );
        }

}

const DishDetail = (props) => {
    const {dish, auth, createComment, createOrder} = props;
    if(!auth.uid) { alert("please signin");
        return <Redirect to ='/home'/>}
    // console.log({dish,comments,auth})
    if (dish != null) {    
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{dish.name}</BreadcrumbItem>
                    </Breadcrumb>
                </div>
                <div className="col-12">
                    <h3>{dish.name}</h3>
                    <hr />
                </div> 
                <div >
                    <RenderDish dish={props.dish} 
                                // favorite={props.favorite} 
                                // postFavorite={props.postFavorite}
                                dishId={props.dish.id}
                                createOrder={createOrder}/>          
                    
                </div>         
                <div className="row">                    
                    <RenderComments comments={props.comments} 
                                    createComment={createComment}
                                    dishId={props.dish.id}  />
                </div>             
            </div>

        )}
    else
        return(
            <div></div>
        )
}
export default DishDetail
  

