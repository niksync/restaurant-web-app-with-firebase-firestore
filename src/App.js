import React, { Component } from 'react';
import { BrowserRouter} from 'react-router-dom'
// import Navbar from './components/layout/Navbar'
// import Dashboard from './components/dashboard/Dashboard'
// import ProjectDetails from './components/projects/ProjectDetails'
// import SignIn from './components/auth/SignIn'
// import SignUp from './components/auth/SignUp'
// import CreateProject from './components/projects/CreateProject'
import Main from './components/MainComponent.1';

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Main/>
        </div>       
      </BrowserRouter>
    );
  }
}

export default App;
