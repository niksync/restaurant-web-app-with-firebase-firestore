const functions = require('firebase-functions');
const admin = require ('firebase-admin')

admin.initializeApp(functions.config().firebase);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
exports.helloWorld = functions.https.onRequest((request, response) => {
 response.send("Hello from Firebase!");
});
const createNotification = (notification =>{
    return admin.firestore().collection('notifications')
    .add(notification)
    .then(doc => console.log('notification added', doc));                        
                           });

exports.orderCreated = functions.firestore
                           .document('orders/{orderId}')
                           .onCreate(doc => {
                                   const order =doc.data();
                                   const notification = {
                                       content: 'Added an order',
                                       user: `${order.authorFirstName} ${order.authorLastName}`,
                                       time: admin.firestore.FieldValue.serverTimestamp()
                                   }
                                   return createNotification(notification)
                           })

exports.feedbackCreated = functions.firestore
                           .document('feedback/{feedbackId}')
                           .onCreate(doc => {
                                   const feedback =doc.data();
                                   const notification = {
                                       content: 'Added feedback',
                                       user: `${feedback.firstname} ${feedback.lastname}`,
                                       time: admin.firestore.FieldValue.serverTimestamp()
                                   }
                                   return createNotification(notification)
                           })
exports.userJoined = functions.auth.user()
                           .onCreate(user => {
                               return admin.firestore().collection('users')
                                   .doc(user.uid).get().then(doc =>{
                                   const newUser = doc.data();
                                   const notification = {
                                       content : "Joined the party",
                                       user: `${newUser.firstName} ${newUser.lastName}`,
                                       time: admin.firestore.FieldValue.serverTimestamp()
                                   }
                                   return createNotification(notification)
                               })
                           })